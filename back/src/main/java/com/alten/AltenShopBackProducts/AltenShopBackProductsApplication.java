package com.alten.AltenShopBackProducts;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(AppConfig.class)
@OpenAPIDefinition(info =
@Info(
		title = "CURD products API",
		version = "0.1",
		description = "API to manage products"
)
)
public class AltenShopBackProductsApplication {

	public static void main(String[] args) {
		SpringApplication.run(AltenShopBackProductsApplication.class, args);
	}

}
