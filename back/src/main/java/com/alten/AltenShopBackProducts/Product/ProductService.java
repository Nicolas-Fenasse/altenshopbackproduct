package com.alten.AltenShopBackProducts.Product;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import jakarta.transaction.Transactional;

@Service
public class ProductService {
    
    private final ProductRepository productRepository;
    private final ModelMapper mapper;

    public ProductService(ProductRepository productRepository, ModelMapper mapper){
        this.mapper = mapper;
        this.productRepository = productRepository;
    }

    public ProductBean get(Long id) {
        Product product = productRepository.getReferenceById(id);
        String productCode = product.getCode();
        return mapper.map(product, ProductBean.class);
    }

    public Set<ProductBean> getAll() {
        List<Product> products = productRepository.findAll();
        return products.stream()
                .map(product -> mapper.map(product, ProductBean.class))
                .collect(Collectors.toSet());
    }

    public void delete(Long id) {
        ProductBean productToDelete = get(id);
        productRepository.deleteById(productToDelete.getId());
    }

    @Transactional
    public ProductBean create(ProductBean productBean) {
        productBean.setId(null);
        Product productToSave = mapper.map(productBean, Product.class);
        Product productSave = productRepository.save(productToSave);
        return mapper.map(productSave, ProductBean.class);
    }

    @Transactional
    public ProductBean update(Long id, ProductBean productBean) {
        //Vérification si le produit existe
        get(id);

        //Mise à jour du produit
        productBean.setId(id);
        Product productToUpdate = mapper.map(productBean, Product.class);
        Product productUpdated = productRepository.save(productToUpdate);
        return mapper.map(productUpdated, ProductBean.class);
    }

}
