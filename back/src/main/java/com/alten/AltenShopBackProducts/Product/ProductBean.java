package com.alten.AltenShopBackProducts.Product;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;

import jakarta.persistence.Column;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

@Getter
@Setter
@NoArgsConstructor
@ToString
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class ProductBean {
    @EqualsAndHashCode.Include
    private Long id;
    private String code;
    private String name;
    private String description;
    private String image;
    private Long price;
    private String category;
    private Integer quantity;
    private String inventoryStatus;
    private String rating;
}
