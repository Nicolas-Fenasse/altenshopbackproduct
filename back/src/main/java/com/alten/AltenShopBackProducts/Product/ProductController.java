package com.alten.AltenShopBackProducts.Product;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.annotation.Resource;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Set;

@RestController
@RequestMapping(value = "api/products",
        produces = MediaType.APPLICATION_JSON_VALUE)
public class ProductController {

    @Resource
    private ProductService productService;

    @Operation(summary = "Get products",
            description = "Retrieve all products")
    @GetMapping(value = "/")
    public Set<ProductBean> getAll() {
        return productService.getAll();
    }

    @Operation(summary = "Get one product",
            description = "Retrieve details for a product")
    @GetMapping(value = "/{id}")
    public ProductBean get(
            @PathVariable(value = "id") Long id) {
        return productService.get(id);
    }

    @Operation(summary = "Delete product",
            description = "Remove a product")
    @DeleteMapping(value = "/{id}")
    public void delete(
            @PathVariable(value = "id") Long id) {
        productService.delete(id);
    }

    @Operation(summary = "Update product",
            description = "Update details of a product if it exists")
    @PatchMapping(value = "/{id}")
    public ProductBean update(
            @PathVariable(value = "id") Long id,
            @RequestBody @Valid ProductBean productBean) {
        return productService.update(id, productBean);
    }

    @Operation(summary = "Create product",
            description = "Create a new product")
    @PostMapping(value = "/")
    public ProductBean create(
            @RequestBody @Valid ProductBean productBean) {
        return productService.create(productBean);
    }
}
