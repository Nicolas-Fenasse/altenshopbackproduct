package com.alten.AltenShopBackProducts;

import com.alten.AltenShopBackProducts.Product.Product;
import com.alten.AltenShopBackProducts.Product.ProductRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.annotation.Resource;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

@Component
public class InitData implements ApplicationRunner{

    @Resource
    private ProductRepository productRepository;

    @Resource
    private ObjectMapper objectMapper;

    @Resource
    private ResourceLoader resourceLoader;


    private static class JsonProducts {
        private List<Product> data;

        public List<Product> getData() {return data;}
    }


    @Override
    @Transactional
    public void run(ApplicationArguments args) throws Exception {

        //Récupération du fichier
        org.springframework.core.io.Resource resource = resourceLoader.getResource("classpath:static/products.json");


        //Lecture du fichier au format String
        String productsJson = new BufferedReader(new InputStreamReader(resource.getInputStream()))
                .lines()
                .collect(Collectors.joining("\n"));

        //Conversion du string en Object
        JsonProducts jsonProducts = objectMapper.readValue(productsJson, JsonProducts.class);

        //Sauvegardes des produits
        productRepository.saveAll(jsonProducts.getData());
    }

    
}
