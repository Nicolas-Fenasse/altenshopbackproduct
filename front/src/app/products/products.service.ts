import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {BehaviorSubject, catchError, map, Observable, tap} from 'rxjs';
import {Product} from './product.class';
import {environment} from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  private static productslist: Product[] = null;
  private products$: BehaviorSubject<Product[]> = new BehaviorSubject<Product[]>([]);

  constructor(private http: HttpClient) {
  }

  getProducts(): Observable<Product[]> {
    if (!ProductsService.productslist) {
      return this.http.get<any>(`${environment.apiUrl}/products/`).pipe(
        map(productList => {
          ProductsService.productslist = productList;
          return ProductsService.productslist.slice();
        }),
        tap(productList => this.products$.next(productList)),
        catchError(error => {
          console.error('Erreur lors de la récupération des produits', error);
          throw error;
        })
      );
    } else {
      this.products$.next(ProductsService.productslist);
      return this.products$;
    }
  }

  create(prod: Product): Observable<Product[]> {
    return this.http.post<any>(`${environment.apiUrl}/products/`, prod).pipe(
      map(productCreated => {
        ProductsService.productslist.push(productCreated);
        return ProductsService.productslist.slice();
      }),
      tap(updatedProductList => this.products$.next(updatedProductList)),
      catchError(error => {
        console.error('Erreur lors de la création du produit', error);
        throw error;
      })
    );
  }

  update(prod: Product): Observable<Product[]> {
    return this.http.patch<any>(`${environment.apiUrl}/products/${prod.id}`, prod)
      .pipe(
        map(productUpdated => {
          const index = ProductsService.productslist.findIndex(element => element.id === prod.id);
          if (index !== -1) {
            ProductsService.productslist[index] = {...productUpdated};
          }
          return ProductsService.productslist.slice();
        }),
        tap(updatedProductList => {
          this.products$.next(updatedProductList);
        }),
        catchError(error => {
          console.error('Erreur lors de la mise à jour du produit', error);
          throw error;
        })
      );
  }


  delete(id: number): Observable<Product[]> {
    return this.http.delete<any>(`${environment.apiUrl}/products/${id}`).pipe(
      map(() => {
          ProductsService.productslist = ProductsService.productslist.filter(value => {
            return value.id !== id;
          });
          return ProductsService.productslist.slice();
        }
      ),
      tap(updatedProductList => this.products$.next(updatedProductList)),
      catchError(error => {
        console.error('Erreur lors de la suppression d\'un produit', error);
        throw error;
      })
    );
  }
}
